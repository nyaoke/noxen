<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>noxen &mdash; noxen website</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

<link rel="stylesheet" href="fonts/icomoon/style.css">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<link rel="stylesheet" href="css/jquery.fancybox.min.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
<link rel="stylesheet" href="css/aos.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    

</head>
<header>
<style>
#tmessage{
  width:100%;
}
.bs-example{
        margin: 20px;
    }
</style>
</header>

<body>
<div class="bs-example">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <a href="#" class="navbar-brand">Welcome..</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav">
                <a href="#" class="nav-item nav-link active"><b>View</b></a>
                <a href="#" class="nav-item nav-link active"><b>All</b></a>
                <a href="#" class="nav-item nav-link active"><b>The</b></a>
                <a href="#" class="nav-item nav-link active" tabindex="-1"><b>Messages</b></a>
            </div>
            <div class="navbar-nav ml-auto">
                <a href="logout.php" class="nav-item nav-link active"><b>Logout</b></a>
            </div>
        </div>
    </nav>
</div>


  <div class="table-responsive">
  <div class="table">
  <table id="tmessage" table>
    <thead>
      <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>message</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    <?php
    require_once("connection.php");
   $sql="select * from messages";
   $result = $conn->query($sql);
   while($row = $result->fetch_assoc()){
       echo "<tr>";
       echo "<td>".$row["id"]."</td>";
       echo "<td>".$row["fname"]."</td>";
       echo "<td>".$row["lname"]."</td>";
       echo "<td>".$row["email"]."</td>";
       echo "<td>".$row["comment"]."</td>";
       echo "<td><a href='delete.php' name='delete'>Delete</a></td>";
   
       echo "</tr>";
       
   }?>
     
    </tbody>
  </table>
  </div>
  </div>
  
   <script src="js2/jquery-3.3.1.min.js"></script>
  <script src="js2/jquery-ui.js"></script>
  <script src="js2/popper.min.js"></script>
  <script src="js2/bootstrap.min.js"></script>
  <script src="js2/owl.carousel.min.js"></script>
  <script src="js2/jquery.magnific-popup.min.js"></script>
  <script src="js/2aos.js"></script>

  <script src="js2/main.js"></script>
  <script src="js/data.js"></script>
  <script src="js/notify.js"></script>

</body>

</html>